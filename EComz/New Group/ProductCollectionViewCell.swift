//
//  ProductCollectionViewCell.swift
//  EComz
//
//  Created by Amelia Laila K on 20/03/23.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblOldPrice: UILabel!
    
    @IBOutlet weak var imgTruck: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblOffTag: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

}
