//
//  Macros.swift
//  Survey
//
//  Created by Amelia Laila K on 06/12/22.
//

import UIKit
import Foundation

//==================================
// MARK: SHOW MESSAGES IN WHOLE APP
//==================================
 
// App Name
let APP_Name = "Survey 2.0"
 
 
// API Key
let API_Key = "5SHJKdXgS5ZURXvM4OPp1F7zl34d1Zt7SbQCoOThUTcLQHyVvnkx2s7owcCyb5"

// No Internet Connection
let k_NO_INTERNET         = "Please check internet connectivity of your device."
let k_NO_SERVER_CONNETCT  = "Something went wrong on server, Please try again."
let K_OOPS_MESSAGE        = "Oops!! Something went wrong. Please try again."
let k_SLOW_INERNET        = "Your internet speed is very slow."
let k_SERVICE_UNABLE      = "Service Unavailable."
let k_USERKEY             = "userInfo"
let k_AUTHORIZATION_FAILD = "Authorization failed."

// Notification use
let k_AppDelegate = UIApplication.shared.delegate as! AppDelegate

// Device Mapping
let SCREEN_WIDTH     =  UIScreen.main.bounds.size.width
let SCREEN_HEIGHT    =  UIScreen.main.bounds.size.height
 
let APP_COLOR  = UIColor.rgb(r: 39, g: 83, b: 187)
let ERROR_COLOR = UIColor.red
let ACCESS_COLOR = UIColor.green

// Simple storage of different data in device
let USER_DEFAULTS  =  UserDefaults.standard
let AUTH_TOKEN = "auth_token"
let REFERESH_TOKEN = "referesh_token"
let TENANT_NAME = "tenant_name"
let FCM_TOKEN = "firebase_token"   // key

let APP_TYPE = "1"
let OS_TYPE = "ios"
let FORCE_UPDATE = "force"
let FLEXIBLE_UPDATE = "flexible"


// Remember credentials or not
let IS_LOGIN = "isLogin"
let IS_WEBVIEW = "isWebView"
let IS_LOGIN_ENABLED = "isEnabled"
let NOTIFI_DATA = "NOTIFICATION_DATA"


let DEPARTMENT_ADMIN = "department-admin"
let LOAD_URL = "load_webview"
let LOAD_URL_ON_TAP_NOTIFICATION = "tap_notification"
let ACCEPT = "Accept"
let ACCEPT_START = "Accept and start"


let kUserNameTextValue = "USERNAME"
let kPasswordTextValue = "PASSWORD"
let kDeviceTokenValue = "DEVICE_TOKEN"
let kFireBaseTokenValue = "FIREBASE_TOKEN"


let NORMAL_PRIORITY = "Normal"
let EMERGENCY_PRIORITY = "Emergency"
let ASSET_TYPE_BED = "bed"
let ASSET_TYPE_OTHER = "other"
 
let APP_FIRST_TIME = "appFirstTime"


//Survey App
let SURVEY_END_TITLE = "Thank you"
let SURVEY_END = "You have reached the end of the Survey"
let SELECTED_LANG = "language"
let IS_FIRST_TIME = "true"
