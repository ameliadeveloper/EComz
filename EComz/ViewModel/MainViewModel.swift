//
//  MainVIewModel.swift
//  EComz
//
//  Created by Amelia Laila K on 17/03/23.
//

import Foundation
import ObjectMapper
class MainViewModel: NSObject {

    func getBanners(callback: @escaping (Bool, BannerModel?, String)-> Void) {

        Service.getApiRequest(method: .getCategories, otherString:  "Type=banners") { (response, error) in
            if response != nil {
                guard let data = response?.value as? [String : Any] else { return }
                callback(true, Mapper<BannerModel>().map(JSON: data ), "success")
            } else {
                callback(false, BannerModel(), "Something went wrong \n Please try again")
            }
        }
    }
    
    func getCategories(callback: @escaping (Bool, CategoryModel?, String)-> Void) {

        Service.getApiRequest(method: .getCategories, otherString:  "Type=category") { (response, error) in
            if response != nil {
                guard let data = response?.value as? [String : Any] else { return }
                callback(true, Mapper<CategoryModel>().map(JSON: data ), "success")
            } else {
                callback(false, CategoryModel(), "Something went wrong \n Please try again")
            }
        }
    }
    
    func getProducts(callback: @escaping (Bool, ProductModel?, String)-> Void) {

        Service.getApiRequest(method: .getCategories, otherString:  "Type=products") { (response, error) in
            if response != nil {
                guard let data = response?.value as? [String : Any] else { return }
                callback(true, Mapper<ProductModel>().map(JSON: data ), "success")
            } else {
                callback(false, ProductModel(), "Something went wrong \n Please try again")
            }
        }
    }
}


