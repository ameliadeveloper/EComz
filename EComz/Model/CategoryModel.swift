//
//  CategoryModel.swift
//  EComz
//
//  Created by Amelia Laila K on 17/03/23.
//

import Foundation
import ObjectMapper


struct CategoryModel: Mappable {
    var status: Bool?
    var category : [Categories]?
    
    init?(map: Map) {
        
    }
    init() {

    }

    mutating func mapping(map: Map) {
        status <- map["status"]
        category <- map["homeData"]
       
    }
    
}

struct Categories: Mappable{
    var type : String?
    var values : [CatObj]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        type <- map["type"]
        values <- map["values"]
    }
    
    
}

struct CatObj: Mappable{
    var id : Int?
    var name : String?
    var image_url : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        image_url <- map["image_url"]
    }
    
    
}
