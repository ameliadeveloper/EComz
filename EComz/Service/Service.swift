//
//  Service.swift
//  EComz
//
//  Created by Amelia Laila K on 17/03/23.
//


import UIKit
import Foundation
import Alamofire
import ObjectMapper

enum HTTPHeaderField: String {
    case authentication  = "Authorization"
    case contentType     = "Content-Type"
    case acceptType      = "Accept"
    case acceptEncoding  = "Accept-Encoding"
    case acceptLangauge  = "Accept-Language"
}

enum ContentType: String {
    case applicationJson = "application/json"
    case multipart       = "multipart/form-data"
    case ENUS            = "en-us"
}

enum HTTPRequestType: String {
    case post = "POST"
    case get = "GET"
}

// SSL pinnnig certificate
// concierge.dev.bestdocapp.in        =   Dev
// concierge.staging.bestdocapp.com   =   Staging
// bestdoc.io                         =   Live


struct HeadURL {
    // Local URL
    fileprivate static let devURL = "https://"
      
    // Live URL
    fileprivate static let liveURL = "https://api."
}

struct Domain {
    // Local URL
    private static let devURL = "https://run.mocky.io/"
    
    // Staging URL
    private static let stagingURL = "https://bdsurvey.dev.bestdoc.us/"
    
    // Live URL
    private static let liveURL = "https://bdsurvey.dev.bestdoc.us/"
}

extension Domain {
    static func baseUrl() -> String {
        return devURL
    }
    
    static func webUrl() -> String {
        return getDomainUrl()
    }
    
    static func getHeadUrl() -> String{
        var nsDictionary: NSDictionary?
            if let path = Bundle.main.path(forResource: "Info", ofType: "plist"){
                nsDictionary = NSDictionary(contentsOfFile: path)
               return (nsDictionary!["HEAD_URL"] as? String ?? "default")
            }
       return ""
    }
    
    static func getDomainUrl() -> String{
        var nsDictionary: NSDictionary?
            if let path = Bundle.main.path(forResource: "Info", ofType: "plist"){
                nsDictionary = NSDictionary(contentsOfFile: path)
               return (nsDictionary!["DOMAIN_URL"] as? String ?? "default")
            }
       return ""
    }
}

class Service: NSObject, URLSessionDelegate {
    
    static func getResponseDTO(_ response:[String:Any]) -> RequestOutcome {
        return Mapper<RequestOutcome>().map(JSON: response)!
    }
    
    static func getErrorMessage(_ message:String = "Please check your internet and try again.") -> RequestOutcome {
        var params:[String:Any] = ["status":false]
        params["message"] = message
        return Service.getResponseDTO(params)
    }
    
    static func setApiHeaders() -> HTTPHeaders {
        var headers: HTTPHeaders = [
            "Content-Type"          : "application/json",
            "os-version"            : "iOS \(UIDevice.current.systemVersion)",
            "device_type"           : "\(UIDevice.modelName)",
            "User-Platform"         : "iOS",  //  "Android"
            "apisid"                : UUID().uuidString,
            "App-Version"           : "1.0.0"
        ]
        
//        let tokenValue = USER_DEFAULTS.value(forKey: AUTH_TOKEN)
//        if (tokenValue != nil) {
//            headers["Authorization"] = "Bearer \(tokenValue!)"
//        }
        return headers
    }
    
    static func getRequest(method: APIEndpoint, otherParam: String!, callback: @escaping (RequestOutcome) -> Void) {
        let endPoint = Domain.baseUrl() + method.rawValue + otherParam
        
        if GlobalUse.isConnectedToNetwork() == true {
            
            AF.request(endPoint, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: setApiHeaders()).validate(statusCode: 200..<500)
                .responseJSON { response in
                    requestCallBack(result: response.result, callback: callback)
                }
            
        } else {
            GlobalUse.showAlert(title: APP_Name, messageString: k_NO_INTERNET)
        }
    }
    
    static func postRequest(method: APIEndpoint, params: [String:Any], callback: @escaping (RequestOutcome) -> Void) {
        
        if GlobalUse.isConnectedToNetwork() == true {
            let endPoint = Domain.baseUrl() + method.rawValue
            print("endPoint %@", endPoint)
            print("params %@", params)
            
            AF.request(endPoint, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: setApiHeaders()).validate(statusCode: 200..<500)
                .responseJSON { response in
                    requestCallBack(result: response.result, callback: callback)
                }
            
        } else {
            GlobalUse.showAlert(title: APP_Name, messageString: k_NO_INTERNET)
        }
    }
    
    
    static func postImgaeRequest(method: APIEndpoint, image: UIImage, params: [String: AnyObject], imgData: NSData?, callback: @escaping (RequestOutcome) -> Void) {
        let parameters = params
        let endPoint = Domain.baseUrl() + method.rawValue
        print(endPoint)
        
        AF.upload(multipartFormData: { multipartFormData in
            
            if imgData != nil {
                for (key,value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                multipartFormData.append(image.jpegData(compressionQuality: 0.4)!, withName: "avatar", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
        },
                  to: endPoint,
                  usingThreshold: UInt64.init(),
                  method: .put,
                  headers: setApiHeaders()) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON { response in
            switch response.result {
            case .success(let resut):
                debugPrint(response)
                print("upload success result: \(String(describing: resut))")
                requestCallBack(result: response.result, callback: callback)
            case .failure(let err):
                print("upload err: \(err)")
                
                let isServerTrustEvaluationError = response.error!.isServerTrustEvaluationError
                if isServerTrustEvaluationError {
                    print("Certificate Pinning Error")
                }
                
            }
        }
    }
    
    private static func requestCallBack(result: Result<Any, AFError>, callback: (RequestOutcome) -> Void) {
        
        switch result {
        case .success(let info):
            //   if let dic = info as? [String : Any] {
            //       callback(getResponseDTO(dic))
            //       return
            //   }
            
            let requestOutcome = Mapper<RequestOutcome>().map(JSON: info as? [String : Any] ?? [:])
            print("###########", requestOutcome as Any)
            if (requestOutcome!.message == nil) {
                requestOutcome!.message = k_NO_SERVER_CONNETCT
            }
            
            callback(requestOutcome!)
            
        case .failure(let error):
            callback(getErrorMessage(error.localizedDescription));
        }
    }
    
    private static func hideHud() {
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                
                if (errSecSuccess == status) {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData);
                        let size = CFDataGetLength(serverCertificateData);
                        let cert1 = NSData(bytes: data, length: size)
                        let file_der = Bundle.main.path(forResource: "staging.bestdocapp.com", ofType: "cer") // name-of-cert-file
                        
                        if let file = file_der {
                            if let cert2 = NSData(contentsOfFile: file) {
                                if cert1.isEqual(to: cert2 as Data) {
                                    completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
                                    return
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // Pinning failed
        print("SSL Pinning Failed ###########")
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
    
}



// ==========================================================================================================
extension Service {
    
    typealias CompletionHandlerGet = (_ response:AFDataResponse<Any>?, _ error:AFError?) -> ()
    typealias CompletionHandlerPost = (_ response:AFDataResponse<Any>?, _ error:AFError?, _ httpResponseCode: Int) -> ()
    
    // MARK:- POST API
    class func postApiRequest(method: APIEndpoint, parameters: [String:Any]?, otherParams: String, completionHandler: CompletionHandlerPost?) {
        
        let endPoint = Domain.baseUrl() + method.rawValue + otherParams
        let urlString = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("URLString ==== ", urlString)
        print("\n==== Body params: ===\n \(parameters ?? [:])")
        print("\n==== Body params dictionary: ===\n \(parameters)")
        k_AppDelegate.showHud()
        if GlobalUse.isConnectedToNetwork() == true {
            
            AF.request(endPoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: setApiHeaders()).validate(statusCode: 200..<600).responseJSON {(response: AFDataResponse<Any>) in
                
                // debugPrint(response)
                
                k_AppDelegate.dissmissHud()
                print("response ========= \n", response)
                
                // Check Status code
                guard let statusCode = response.response?.statusCode else { return }
                
                if statusCode == 401 {
                    completionHandler!(nil, nil, statusCode)
                } else if statusCode == 403 || statusCode == 500 {
                    completionHandler!(nil, nil, statusCode)
                } else
                if statusCode == 200 {
                    switch(response.result) {
                    case .success(_):
                        // Do Somethings
                        completionHandler!(response, nil, statusCode)
                    case .failure(_):
                        // GlobalUse.showAlert(title: APP_Name, messageString: k_SERVICE_UNABLE)
                        
                        let isServerTrustEvaluationError = response.error!.isServerTrustEvaluationError
                        if isServerTrustEvaluationError {
                            print("Certificate Pinning Error")
                        }
                        completionHandler!(nil, response.error!, statusCode)
                        break
                    }
                }
                else {
                    // GlobalUse.showAlert(title: APP_Name, messageString: response.description)
                    completionHandler!(response, nil, statusCode)
                }
            }
            
        } else {
            k_AppDelegate.dissmissHud()
            GlobalUse.showAlert(title: APP_Name, messageString: k_NO_INTERNET)
        }
        
    }
    
    // MARK:- GET API
    class func getApiRequest(method: APIEndpoint, otherString: String, completionHandler: @escaping CompletionHandlerGet) {
        
        let endPoint = Domain.baseUrl() + method.rawValue + otherString
        let urlString = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("UrlString ===== ", urlString)
        print("\n==== Headers ===\n ", setApiHeaders())
        k_AppDelegate.showHud()
        if GlobalUse.isConnectedToNetwork() == true {
            
            AF.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {(response: AFDataResponse<Any>) in
                
                
                // debugPrint(response)
                
                k_AppDelegate.dissmissHud()
                print("response ========= \n", response)
                
                let status_code = response.response?.statusCode
                if status_code == 200 {
                    switch(response.result) {
                    case .success(_):
                        completionHandler(response, nil)
                    case .failure(_):
                        // print(response.error!)
                        let isServerTrustEvaluationError = response.error!.isServerTrustEvaluationError
                        if isServerTrustEvaluationError {
                            print("Certificate Pinning Error")
                        }
                        completionHandler(nil, response.error!)
                        break
                    }
                } else {
                    completionHandler(response, nil)
                }
            }
            
        } else {
            k_AppDelegate.dissmissHud()
            GlobalUse.showAlert(title: APP_Name, messageString: k_NO_INTERNET)
        }
    }
    
    
    class func putApiRequest(method: APIEndpoint, parameters: [String: Any]?, otherParams: String, completionHandler: CompletionHandlerPost?) {
        
        let endPoint = Domain.baseUrl() + method.rawValue + otherParams
        let urlString = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("URLString ==== ", urlString)
        print("\n==== Body params: ===\n \(parameters ?? [:])")
        k_AppDelegate.showHud()
        if GlobalUse.isConnectedToNetwork() == true {
            
            AF.request(endPoint, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: setApiHeaders()).validate(statusCode: 200..<500).responseJSON {(response: AFDataResponse<Any>) in
                
                // debugPrint(response)
                
                k_AppDelegate.dissmissHud()
                print("response ========= \n", response)
                
                // Check Status code
                guard let statusCode = response.response?.statusCode else { return }
                
                if statusCode == 401 {
                    completionHandler!(nil, nil, statusCode)
                } else
                if statusCode == 200  {
                    switch(response.result) {
                    case .success(_):
                        // Do Somethings
                        completionHandler!(response, nil, statusCode)
                    case .failure(_):
                        // GlobalUse.showAlert(title: APP_Name, messageString: k_SERVICE_UNABLE)
                        
                        let isServerTrustEvaluationError = response.error!.isServerTrustEvaluationError
                        if isServerTrustEvaluationError {
                            print("Certificate Pinning Error")
                        }
                        completionHandler!(response, response.error!, statusCode)
                    }
                }
                else {
                    GlobalUse.showAlert(title: APP_Name, messageString: k_SERVICE_UNABLE)
                    completionHandler!(response, nil, statusCode)
                }
            }
            
        } else {
            k_AppDelegate.dissmissHud()
            GlobalUse.showAlert(title: APP_Name, messageString: k_NO_INTERNET)
        }
        
    }
    
    
    // MARK:- Uplaod Image/Video API
    class func uploadImageVideo(data: Data, method: APIEndpoint, mimeType: String, completion: @escaping (Bool?, String?) -> Void) {
        
        if GlobalUse.isConnectedToNetwork() == true {
            
            let endPoint = Domain.baseUrl() + method.rawValue
            let urlString = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let requestURL = URL(string: urlString)!
            
            print("urlString ==== ", urlString)
            var request = URLRequest(url: requestURL)
            request.httpMethod = "PUT"
            request.httpBody = data
            request.setValue(mimeType, forHTTPHeaderField: "Content-Type")
            request.setValue("\(data.count)", forHTTPHeaderField: "Content-Length")
            request.setValue("public-read", forHTTPHeaderField: "x-amz-acl")
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    completion(false, error.localizedDescription)
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    return
                }
                
                if httpResponse.statusCode == 200 {
                    completion(true, nil)
                }
                
                else {
                    completion(false, nil)
                }
            }
            task.resume()
            
        } else {
            
            GlobalUse.showAlert(title: APP_Name, messageString: k_NO_INTERNET)
        }
    }
    
    class func postImgaeRequestAPI(method: APIEndpoint, arrImage:[UIImage], params: [String: Any], imageName: String, mimeType: String, callback: CompletionHandlerPost?) {
        let parameters = params
        let endPoint = Domain.baseUrl() + method.rawValue
        let urlString = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("URLString ==== ", urlString)
        print("\n==== Body params: ===\n \(params)")
        
        k_AppDelegate.showHud()
        if GlobalUse.isConnectedToNetwork() == true {
            AF.upload(multipartFormData: { multipartFormData in
                for (key,value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                for img in arrImage {
                    guard let imgData = img.jpegData(compressionQuality: 0.4) else { return }
                    multipartFormData.append(imgData, withName: "prescription", fileName: imageName, mimeType: mimeType)
                }
            },
                      to: urlString,
                      method: .post,
                      headers: setApiHeaders()) .uploadProgress(queue: .main, closure: { progress in
                print("Upload Progress: \(progress.fractionCompleted)")
            }).responseJSON { response in
                
                k_AppDelegate.dissmissHud()
                // Check Status code
                guard let statusCode = response.response?.statusCode else { return }
                
                switch response.result {
                case .success(let resut):
                    debugPrint(response)
                    print("upload success result: \(String(describing: resut))")
                    callback!(response, response.error!, statusCode)
                case .failure(let err):
                    print("upload err: \(err)")
                    
                    let isServerTrustEvaluationError = response.error!.isServerTrustEvaluationError
                    if isServerTrustEvaluationError {
                        print("Certificate Pinning Error")
                    }
                    callback!(nil, response.error!, statusCode)
                    break
                }
            }
        } else {
            k_AppDelegate.dissmissHud()
            GlobalUse.showAlert(title: APP_Name, messageString: k_NO_INTERNET)
        }
    }
    
    //MARK: Fetch message for chat use only
    static func requestApi(service: String, parameters: NSDictionary, requestType: HTTPRequestType, completionHandler: @escaping (Data?, Error?) -> Void){
        
        do {
            
            let request : NSMutableURLRequest = NSMutableURLRequest.init()
            
            if requestType == .post {
                let url = URL.init(string: service)!
                let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                request.url = url
                // insert json data to the request
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.httpMethod = requestType.rawValue
                request.httpBody = jsonData
                
            }
            
            else if(requestType == .get) {
                var components = URLComponents(string: service)!
                components.queryItems = parameters.map { URLQueryItem(name: $0 as! String, value: $1 as? String) }
                
                components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
                request.url = components.url
                
                print("Request URL ==== ", request.url!.absoluteString)
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                guard let data = data,                            // is there data
                      let response = response as? HTTPURLResponse,  // is there HTTP response
                      (200 ..< 600) ~= response.statusCode,         // is statusCode 2XX
                      error == nil else {                           // was there no error, otherwise ...
                          completionHandler(nil, error)
                          return
                      }
                print("Request response == ", response)
                completionHandler(data, nil)
            }
            
            task.resume()
            // return task
        } catch {
            print(error)
        }
    }
    
    
    // MARK:- PATCH API
    class func patchApiRequest(method: APIEndpoint, parameters: [String:Any]?, otherParams: String, completionHandler: CompletionHandlerPost?) {
        
        let endPoint = Domain.baseUrl() + method.rawValue + otherParams
        let urlString = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("URLString ==== ", urlString)
        print("\n==== Body params: ===\n \(parameters ?? [:])")
        k_AppDelegate.showHud()
        if GlobalUse.isConnectedToNetwork() == true {
            
            AF.request(endPoint, method: .patch, parameters: parameters, encoding: JSONEncoding.default, headers: setApiHeaders()).validate(statusCode: 200..<500).responseJSON {(response: AFDataResponse<Any>) in
                
                k_AppDelegate.dissmissHud()
                print("response ========= \n", response)
                
                
                // Check Status code
                guard let statusCode = response.response?.statusCode else { return }
                
                if statusCode == 401 {
                    completionHandler!(nil, nil, statusCode)
                } else
                if statusCode == 200 {
                    switch(response.result) {
                    case .success(_):
                        // Do Somethings
                        completionHandler!(response, nil, statusCode)
                    case .failure(_):
                        // GlobalUse.showAlert(title: APP_Name, messageString: k_SERVICE_UNABLE)
                        
                        let isServerTrustEvaluationError = response.error!.isServerTrustEvaluationError
                        if isServerTrustEvaluationError {
                            print("Certificate Pinning Error")
                        }
                        completionHandler!(nil, response.error!, statusCode)
                        break
                    }
                }
                else {
                    GlobalUse.showAlert(title: APP_Name, messageString: k_NO_SERVER_CONNETCT)
                    completionHandler!(response, nil, statusCode)
                }
            }
            
        } else {
            k_AppDelegate.dissmissHud()
            GlobalUse.showAlert(title: APP_Name, messageString: k_NO_INTERNET)
        }
    }
    
}


// MARK:- API'S EndPoint

public enum APIEndpoint: String {
    case getCategories                 = "v3/69ad3ec2-f663-453c-868b-513402e515f0/"
    case getUnitDetails            = "unit/retrieve/unit-details?"
    case submitResponse             = "survey/v2/submit-feedback"
    case userAuthentication        = "/api/v1/auth/user"
    case updateFirebaseToken       = "/api/v1/auth/updateFirebase"
    case updateRefershToken       = "/api/v1/auth/refresh"
}


