//
//  ServiceWebCallManager.swift
//  Survey
//
//  Created by Amelia Laila K on 06/12/22.
//

import UIKit
import Alamofire

enum HTTPBodyRequestType: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case patch = "PATCH"
}

class ServiceWebCallManager: NSObject {
    
    static func request(service: String, parameters: NSDictionary, requestType: HTTPBodyRequestType, completionHandler: @escaping ([String: Any]?, Error?) -> Void){
        
        do {
            
            let request : NSMutableURLRequest = NSMutableURLRequest.init()
            if requestType == .post {
                let url = URL.init(string: service)!
                let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                request.url = url
                // insert json data to the request
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.httpMethod = requestType.rawValue
                request.httpBody = jsonData
                
            }
            
            else if (requestType == .get) {
                var components = URLComponents(string: service)!
                components.queryItems = parameters.map { (arg) -> URLQueryItem in
                    
                    let (key, value) = arg
                    return URLQueryItem(name: key as! String, value: (value as! String))
                }
                components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
                request.url = components.url
            }
                        
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                guard let data = data,                            // is there data
                      let response = response as? HTTPURLResponse,  // is there HTTP response
                      (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                      error == nil else {                           // was there no error, otherwise ...
                    completionHandler(nil, error)
                    return
                }
                
                let responseObject = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any]
                completionHandler(responseObject, nil)
            }
            
            task.resume()
            // return task
        } catch {
            print(error)
        }
    }
    
    
    static func requestApi(service: String, parameters: NSDictionary, requestType: HTTPBodyRequestType, completionHandler: @escaping (Data?, Error?) -> Void){
        
        do {
 
            let request : NSMutableURLRequest = NSMutableURLRequest.init()
            if requestType == .post {
                
                let url = URL.init(string: service)!
                let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                request.url = url
                
                // insert json data to the request
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
//                request.addValue("Bearer \(UserDefaults.standard.value(forKey: APNS_AUTH_TOKEN) ?? "")", forHTTPHeaderField: "Authorization")
                request.httpMethod = requestType.rawValue
                request.httpBody = jsonData

                print("POST API URL == ", url)
                
                let jsonParams = String(data: jsonData, encoding: String.Encoding.utf8)
                print("Params = ", jsonParams as Any)
                
            }
            
            else if(requestType == .get) {
                var components = URLComponents(string: service)!
                components.queryItems = parameters.map { URLQueryItem(name: $0 as! String, value: $1 as? String) }
                
                //       parameters.map { (arg) -> URLQueryItem in
                //       print("Params ==", arg)
                //       let (key, value) = arg
                //       return URLQueryItem(name: key as! String, value: (value as! String))
                //     }
                components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
                request.url = components.url
                
                print("Request URL == ", request.url!.absoluteString)
            }
            
            else if(requestType == .put) {
                
                guard let putUrl = URL(string: service) else {
                    print("Error: cannot create URL")
                    return
                }
                
                let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                request.url = putUrl
                request.setValue("application/js21on; charset=utf-8", forHTTPHeaderField: "Content-Type")
//                request.addValue("Bearer \(UserDefaults.standard.value(forKey: APNS_AUTH_TOKEN) ?? "")", forHTTPHeaderField: "Authorization")
                request.httpMethod = requestType.rawValue
                request.httpBody = jsonData
                
                print("PUT API URL == ", putUrl)
                
                let jsonParams = String(data: jsonData, encoding: String.Encoding.utf8)
                print("Params == ", jsonParams as Any)
                                         
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                
                guard let data = data,                            // is there data
                      let response = response as? HTTPURLResponse,  // is there HTTP response
                      (200 ..< 600) ~= response.statusCode,         // is statusCode 2XX
                      error == nil else {                           // was there no error, otherwise ...
                    completionHandler(nil, error)
                    return
                }
                // print("Request response == ", response)
                let Response = String(data: data, encoding: String.Encoding.utf8)
                print("Api response == ", Response as Any)
                
                completionHandler(data, nil)
            }
            
            task.resume()
            // return task
        } catch {
            print(error)
            completionHandler(nil, error)
        }
        
    }
    
    
    static func requestPutApi(service: String, parameters: NSDictionary, requestType: HTTPBodyRequestType, completionHandler: @escaping (Data?, Error?) -> Void){
        
        do {
            let request : NSMutableURLRequest = NSMutableURLRequest.init()
            if(requestType == .put) {
                var components = URLComponents(string: service)!
                components.queryItems = parameters.map { URLQueryItem(name: $0 as! String, value: $1 as? String) }
                components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
                request.url = components.url
                request.httpMethod = requestType.rawValue
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                
                guard let data = data,                             // is there data
                      let response = response as? HTTPURLResponse,  // is there HTTP response
                      (200 ..< 600) ~= response.statusCode,         // is statusCode 2XX
                      error == nil else {                           // was there no error, otherwise ...
                    completionHandler(nil, error)
                    return
                }
                print("Request response == ", response)
                completionHandler(data, nil)
            }
            
            task.resume()
            // return task
        }
        
    }
    
    
    typealias CompletionHandlerPost = (_ response:AFDataResponse<Any>?, _ error:AFError?, _ httpResponseCode: Int) -> ()
    
    static func postApiWithAlamofire(service: String, parameters: [String: Any], requestType: HTTPBodyRequestType, completionHandler: CompletionHandlerPost?) {
         
        print("==== API URL === \(service)")
        print("\n==== Body params: ===\n \(parameters)")
        
        AF.request(URL.init(string: service)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print(response.result)
            
            print("response ========= \n", response)
            
            guard let statusCode = response.response?.statusCode else { return }
            if statusCode == 401 {
                completionHandler!(nil, nil, statusCode)
            } else
            if statusCode == 200 {
            
                switch response.result {
                case .success(_):
                    // if let json = response.value {
                    completionHandler!(response, nil, statusCode)
                    // }
                    break
                case .failure(_):
                    completionHandler!(nil, response.error!, statusCode)
                    break
                }
            } else {
                completionHandler!(response, nil, statusCode)
            }
        }
        
    }
    
    
    static func patchApiWithAlamofire(service: String, parameters: [String: Any]?, completionHandler: CompletionHandlerPost?) {
         
        print("==== API URL === \(service)")
        print("\n==== Body params: ===\n \(parameters ?? [:])")
        
        AF.request(URL.init(string: service)!, method: .patch, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print(response.result)
            
            print("response ========= \n", response)
            
            guard let statusCode = response.response?.statusCode else { return }
            if statusCode == 401 {
                completionHandler!(nil, nil, statusCode)
            } else
            if statusCode == 200 {
            
                switch response.result {
                case .success(_):
                    // if let json = response.value {
                    completionHandler!(response, nil, statusCode)
                    // }
                    break
                case .failure(_):
                    completionHandler!(response, response.error!, statusCode)
                    break
                }
            } else {
                completionHandler!(response, nil, statusCode)
            }
        }
        
    }
 
}

