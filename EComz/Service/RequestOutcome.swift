//
//  RequestOutcome.swift
//  Survey
//
//  Created by Amelia Laila K on 06/12/22.
//


import UIKit
import ObjectMapper

class RequestOutcome: NSObject, Mappable {

    var status: Bool?
    var message : String?
    var data    : Any?
    var code    : Int?
    var success = false
    var internetAvailable = true
        
    func isSuccess() -> Bool {
        return status != nil && status == true;
    }
    func getMessage() -> String {
        return message ?? ""
    }
    
    override init() {}
    
    init(status: Bool = false
        , internetAvailable: Bool = true
        , data: Any? = nil
        , message: String? = nil) {
        
        self.status = status
        self.data = data
        self.message = message
    }
    
    required init(map: Map) {
        
    }
    
    // Mapping
    func mapping(map: Map) {
        status  <- map["status"]
        data    <- map["data"]
        message <- map["message"]
        code    <- map["statusCode"]
    }
    
    
}
