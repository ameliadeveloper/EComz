//
//  CategoryCollectionViewCell.swift
//  EComz
//
//  Created by Amelia Laila K on 20/03/23.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var topView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        topView.layer.cornerRadius = topView.layer.bounds.width / 2
        topView.clipsToBounds = true
        // Initialization code
    }

}
