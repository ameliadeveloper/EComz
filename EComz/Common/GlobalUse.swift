//
//  GlobalUse.swift
//  EComz
//
//  Created by Amelia Laila K on 17/03/23.
//

import UIKit
import AVFoundation
import SystemConfiguration

class GlobalUse: NSObject {
    
    //==============================
    // Show Alert in entire project
    //==============================
    class func showAlert(title titleString: String, messageString strMessage: String) {
        let alert:UIAlertController = UIAlertController.init(title: titleString , message: strMessage, preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = APP_COLOR
        
        let imgView = UIImageView.init(image: UIImage.init(named: "logo-c"))
        // imgView.frame = CGRect.init(x: (alert.view.frame.width / 2) , y: 20, width: 20, height: 20)
        imgView.frame = CGRect.init(x: 70 , y: 20, width: 20, height: 20)
        imgView.clipsToBounds = true
        alert.view.layoutIfNeeded()
        alert.view.addSubview(imgView)
        
        let alertAction:UIAlertAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
        }
        alert.addAction(alertAction)
        // UIApplication.shared.keyWindow?.windowLevel = UIWindow.Level.alert + 1
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        
    }
    
    class func showAlertWithAction(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
        
    
    //====================================
    // MARK:- INTERNET AVAILABLE OR NOT
    //====================================
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
        
    class func generateThumbnail(url: URL, completion: @escaping (UIImage) -> Swift.Void)  {
        do {
            let asset = AVURLAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            let time = CMTimeMake(value: 1, timescale: 2)
            let cgImage = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            completion(UIImage(cgImage: cgImage))
        } catch {
            print(error.localizedDescription)
            completion(UIImage.init())
        }
    }
    
//    public static func saveUserInfo(user : UserInfoDto) {
//        let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)
//        USER_DEFAULTS.set(encodedData, forKey: k_USERKEY)
//        USER_DEFAULTS.synchronize()
//    }
//
//    public static func fetchUserInfo() -> UserInfoDto {
//        if let decoded = USER_DEFAULTS.object(forKey: k_USERKEY) {
//            let user: UserInfoDto = NSKeyedUnarchiver.unarchiveObject(with: decoded as! Data) as! UserInfoDto;
//            return user
//        } else {
//            return UserInfoDto()
//        }
//    }
    
//    public static func removeUser() {
//        USER_DEFAULTS.removeObject(forKey: k_USERKEY)
//        USER_DEFAULTS.set(nil, forKey: k_USERKEY)
//        USER_DEFAULTS.synchronize()
//    }
    
//    public static func removaAllUserDefaultsKeys() {
//        if let appDomain = Bundle.main.bundleIdentifier {               // Remove all Keys
//            USER_DEFAULTS.removePersistentDomain(forName: appDomain)
//        }
//    }
     
   public class func isObjectNotNil(object:AnyObject!) -> Bool
    {
        if let _:AnyObject = object
        {
            return true
        }

        return false
    }
    class func timeConversion12(time24: String) -> String {
        let dateAsString = time24
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        
        let date = df.date(from: dateAsString)
        df.dateFormat = "h:mm a"
        
        let time12 = df.string(from: date ?? Date())
        return time12
    }
        
    
    
    static func calculateAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        return age!
    }
    
    public static func fillVersionDetails()->String{
        let versionInfo = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        
        let version = "Version " + versionInfo + getVarient() + getInstance()
        print("Version- "+version)
        return version
    }
    
    public static func getVarient() -> String{
        var nsDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"){
            nsDictionary = NSDictionary(contentsOfFile: path)
            let varient = (nsDictionary!["VARIENT"] as? String ?? "")
            print("Version- let varient",varient)
            if varient != ""{
                return ("_"+varient)
            }else{
                return ""
            }
        }
        return ""
    }
    
    
    public static func getInstance() -> String{
        var nsDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"){
            nsDictionary = NSDictionary(contentsOfFile: path)
           let instance = (nsDictionary!["INSTANCE"] as? String ?? "")
            if instance != ""{
                return ("_"+instance)
            }else{
                return ""
            }
        }
        return ""
    }
    
}
 


// MARK: Subclass of UILabel
class CustomLabel: UILabel {
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 3.0, left: 5.0, bottom: 2.0, right: 3.0)
        super.drawText(in: rect.inset(by: insets))
    }
     
}


// =================================
class CircleLabel: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        self.layer.cornerRadius = self.bounds.width/2
        self.clipsToBounds = true
        self.textColor = UIColor.white
        self.setProperties(borderWidth: 1.0, borderColor:UIColor.black)
    }
    func setProperties(borderWidth: Float, borderColor: UIColor) {
        self.layer.borderWidth = CGFloat(borderWidth)
        self.layer.borderColor = borderColor.cgColor
    }
}

// @IBOutlet weak var myCustomLabel: CircleLabel!   //// How to use

extension String {
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}

extension NSDictionary {
    var swiftDictionary: Dictionary<String, Any> {
        var swiftDictionary = Dictionary<String, Any>()

        for key : Any in self.allKeys {
            let stringKey = key as! String
            if let keyValue = self.value(forKey: stringKey){
                swiftDictionary[stringKey] = keyValue
            }
        }

        return swiftDictionary
    }
}


class Helper{
    static func initActivityIndicator(activityIndicator : UIActivityIndicatorView){
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.large
        activityIndicator.startAnimating()
    }
    
    
}
