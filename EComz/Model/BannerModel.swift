//
//  CategoryModel.swift
//  EComz
//
//  Created by Amelia Laila K on 17/03/23.
//

import Foundation
import ObjectMapper


struct BannerModel: Mappable {
    var status: Bool?
    var banner : [Banners]?
    
    init?(map: Map) {
        
    }
    init() {

    }

    mutating func mapping(map: Map) {
        status <- map["status"]
        banner <- map["homeData"]
       
    }
    
}

struct Banners: Mappable{
    var type : String?
    var values : [BanObj]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        type <- map["type"]
        values <- map["values"]
    }
    
    
}

struct BanObj: Mappable{
    var id : Int?
    
    var banner_url : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
       
        banner_url <- map["banner_url"]
    }
    
    
}
