//
//  ViewController.swift
//  EComz
//
//  Created by Amelia Laila K on 17/03/23.
//

import UIKit
import Foundation

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   
    
    fileprivate let objMainViewModel = MainViewModel()
    @IBOutlet weak var bannerCV: UICollectionView!
    @IBOutlet weak var categoryCV: UICollectionView!
    @IBOutlet weak var productCV: UICollectionView!
    var objBannersViewModel = BannerModel()
     var objCategoryModel = CategoryModel()
     var objProductModel = ProductModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        getCategories()
        getBanners()
        getProducts()
    }

    
    
    func initUI(){
        bannerCV.registerCell(nib: "BannersCollectionViewCell", reuseIdentifier: "BannersCollectionViewCell")
        bannerCV.delegate = self
        bannerCV.dataSource = self
        bannerCV.reloadData()
        categoryCV.registerCell(nib: "CategoryCollectionViewCell", reuseIdentifier: "CategoryCollectionViewCell")
        categoryCV.delegate = self
        categoryCV.dataSource = self
        categoryCV.reloadData()
        productCV.registerCell(nib: "ProductCollectionViewCell", reuseIdentifier: "ProductCollectionViewCell")
        productCV.delegate = self
        productCV.dataSource = self
        productCV.reloadData()
    }
    
    func getBanners() {
        self.objMainViewModel.getBanners{ flag, obj, message in
            if flag {
                self.objBannersViewModel = obj ?? BannerModel()
                self.bannerCV.reloadData()
            }
        }
    }
    
    func getCategories() {
        self.objMainViewModel.getCategories{ flag, obj, message in
            if flag {
                self.objCategoryModel = obj ?? CategoryModel()
                self.categoryCV.reloadData()
            }
        }
    }
    
    func getProducts() {
        self.objMainViewModel.getProducts{ flag, obj, message in
            if flag {
                self.objProductModel = obj ?? ProductModel()
                self.productCV.reloadData()
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView{
        case bannerCV:
            return objBannersViewModel.banner?[1].values?.count ?? 0
        case categoryCV:
            return objCategoryModel.category?[0].values?.count ?? 0
        case productCV:
            return objProductModel.category?[2].values?.count ?? 0
        default:
            return objBannersViewModel.banner?[1].values?.count ?? 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView{
        case bannerCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannersCollectionViewCell", for: indexPath) as! BannersCollectionViewCell
            if let imgUrl =  objBannersViewModel.banner?[1].values?[indexPath.row].banner_url{
                cell.imgView.imageFromUrl(urlString: imgUrl)
            }
            
            return cell
        case categoryCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
            if let imgUrl =  objCategoryModel.category?[0].values?[indexPath.row].image_url{
                cell.imgView.imageFromUrl(urlString: imgUrl)
            }
            cell.lblName.text = objCategoryModel.category?[0].values?[indexPath.row].name
            return cell
            
        case productCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
            if let imgUrl =  objProductModel.category?[2].values?[indexPath.row].image_url{
                print("imgUrl",imgUrl)
                cell.imgView.imageFromUrl(urlString: imgUrl)
            }
            
            let actualPrice = objProductModel.category?[2].values?[indexPath.row].actual_price
            cell.lblDesc.text = objProductModel.category?[2].values?[indexPath.row].name
            cell.lblPrice.text = objProductModel.category?[2].values?[indexPath.row].offer_price

            if let offPrice = objProductModel.category?[2].values?[indexPath.row].offer{
                if offPrice == 0{
                    cell.lblOldPrice.text = " "
                    cell.topView.isHidden = true
                    
                }else{
                    cell.lblOffTag.text = String(offPrice) + "%"
                    let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: actualPrice ?? "")
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
                    cell.lblOldPrice.attributedText = attributeString
                }
            }
            
            if let isExpress = objProductModel.category?[2].values?[indexPath.row].is_express{
                if !isExpress{
                    cell.imgTruck.isHidden = true
                }
            }
            return cell
            
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannersCollectionViewCell", for: indexPath) as! BannersCollectionViewCell
            if let imgUrl =  objBannersViewModel.banner?[1].values?[indexPath.row].banner_url{
                cell.imgView.imageFromUrl(urlString: imgUrl)
                
            }
            
            return cell
        }
       

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView{
        case bannerCV:
            return CGSize(width: self.view.frame.width, height: self.view.frame.height)
        case categoryCV:
            return CGSize(width: (self.view.frame.width)/6, height: self.view.frame.height)


        default:
            return CGSize(width: self.view.frame.width, height: self.view.frame.height)

        }
        }

}


