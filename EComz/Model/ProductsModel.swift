//
//  CategoryModel.swift
//  EComz
//
//  Created by Amelia Laila K on 17/03/23.
//

import Foundation
import ObjectMapper


struct ProductModel: Mappable {
    var status: Bool?
    var category : [Products]?
    
    init?(map: Map) {
        
    }
    init() {

    }

    mutating func mapping(map: Map) {
        status <- map["status"]
        category <- map["homeData"]
       
    }
    
}

struct Products: Mappable{
    var type : String?
    var values : [ProdObj]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        type <- map["type"]
        values <- map["values"]
    }
    
    
}

struct ProdObj: Mappable{
    var id : Int?
    var name : String?
    var image_url : String?
    var actual_price : String?
    var offer_price : String?
    var offer : Int?
    var is_express : Bool?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        image_url <- map["image"]
        actual_price <- map["actual_price"]
        offer_price <- map["offer_price"]
        offer <- map["offer"]
        is_express <- map["is_express"]
    }
    
    
}
